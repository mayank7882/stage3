import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {
    static int find(String [] strings,String q){
        int count=0;
        for(int i=0;i<strings.length;i++){
            if(strings[i].equals(q)){
                count++;
            }
        }
        return count;
    }
    // Complete the matchingStrings function below.
    static int[] matchingStrings(String[] strings, String[] queries) {
        int[] brr=new int[queries.length];
        for(int i=0;i<queries.length;i++){
            brr[i]=find(strings,queries[i]);
            //System.out.print(brr[i]+",");
        }

        return brr;
    }

    

    public static void main(String[] args) {
        Scanner kb=new Scanner(System.in);
        int n=kb.nextInt();
        String strings[]= new String[n];
        for(int i=0;i<n;i++){
            strings[i]=kb.next();
        }
        int q=kb.nextInt();
        String [] queries=new String[q];
        for(int i=0;i<q;i++){
            queries[i]=kb.next();
        }
        int[] ans=new int[q];
        ans=matchingStrings(strings,queries);
        for(int x:ans){
            System.out.println(x);
        }
    }
}
